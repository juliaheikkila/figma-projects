# Figma Projects

This README.md contains links to my Figma projects.

## Oulu2026 Kandidaattiprojekti

This project was done in our Kandidaattiprojekti-course, which was a part of my B.Sc. studies in Information Processing Science in University of Oulu.

Together in our project team, we designed (and developed) a mobile application for the Oulu2026 culture programme. Our task and goal was to present the food culture and different cities in the Oulu2026-area.

Link to the Figma project: https://www.figma.com/file/M4QDIWyYZK9Q9r5ZSy4hMk/Oulu2026---Kandidaattiprojekti?type=design&node-id=0%3A1&mode=design&t=ZkCB6UUDH14HBfaN-1

## Human-computer interaction course project

This project was a part of my course work in the Human-computer Interaction course that I completed during Fall 2023.

I designed a mobile application which is a student event calendar & ticketing platform for student events. We were instructed to use the Material 3 Design Kit.

Link to the Figma project: https://www.figma.com/file/uJrU4tRJg2e54O34hqav7v/HCI-Course-project---Julia-Heikkil%C3%A4?type=design&t=bAypsDKAP8GqiPgZ-6

## User Experience (UX) Design and Management
 
This project was done as a group work in the User Experience (UX) Design and Management course in Spring 2024.

The mobile application that we designed is continuation and improved version of the project above, Student event calendar application. 

Link to the Figma project: https://www.figma.com/design/TZbBzhqDH5TIcqMXO0m82G/Student-event-calendar-app?m=auto&t=PklldUhvDOg69MTf-6
